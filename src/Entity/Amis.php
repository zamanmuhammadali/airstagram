<?php

namespace App\Entity;

use App\Repository\AmisRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=AmisRepository::class)
 */
class Amis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *@Groups({"amis"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="amis",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_source;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="user")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"amis"})
     */
    private $user_target;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"amis"})
     *
     * 0 si il s'agit d'invitation
     * 1 si l'utilisateur target a accepter l'invitation et ils sont devenu ami
     *
     */
    private $accepter;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserSource(): ?User
    {
        return $this->user_source;
    }

    public function setUserSource(?User $user_source): self
    {
        $this->user_source = $user_source;

        return $this;
    }

    public function getUserTarget(): ?User
    {
        return $this->user_target;
    }

    public function setUserTarget(?User $user_target): self
    {
        $this->user_target = $user_target;

        return $this;
    }

    public function getAccepter(): ?int
    {
        return $this->accepter;
    }

    public function setAccepter(?int $invitation_accepter): self
    {
        $this->accepter = $invitation_accepter;

        return $this;
    }
}
