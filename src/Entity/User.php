<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Vich\Uploadable
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"amis","commentaire"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Assert\Email(
     *     message="email non valide"
     * )
     * @Groups({"amis","commentaire"})
     */

    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="le mot de passe ne doit pas etre null")
     * @Assert\Length(min="8",minMessage="le mot de passe doit contenir au moins 8 caractères")
     */
    private $password;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"amis","commentaire"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"amis","commentaire"})
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"amis","commentaire"})
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="l'adresse ne doit pas etre null ")
     * @Groups({"amis"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="datetime",options={"default": "CURRENT_TIMESTAMP"}))
     * @Groups({"amis"})
     */
    private $dateNaiss;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="profile_image", fileNameProperty="image")
     *
     * @var File|null
     * @Groups({"amis"})
     */
    public $imageFile;
    /**
     * @ORM\Column(type="datetime",options={"default": "CURRENT_TIMESTAMP"}))
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface|null $updatedAt
     * @return User
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): User
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"amis","commentaire"})
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity=Profil::class, inversedBy="user")
     * @ORM\JoinColumn(nullable=false)
     */
    private $profil;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="user")
     */
    private $photos;

    /**
     * @ORM\OneToMany(targetEntity=Album::class, mappedBy="user")
     */
    private $albums;

    /**
     * @ORM\OneToMany(targetEntity=Amis::class, mappedBy="user_source",cascade={"remove"})
     */
    private $amis;

    /**
     * @ORM\OneToMany(targetEntity=Amis::class, mappedBy="user_target",cascade={"remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="user")
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity=Signaler::class, mappedBy="user")
     */
    private $signalers;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $compte;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->amis = new ArrayCollection();
        $this->user = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->signalers = new ArrayCollection();

    }

    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_' . strtoupper($this->getProfil()->getLibelle());

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function setID(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getDateNaiss(): ?\DateTimeInterface
    {
        return $this->dateNaiss;
    }

    public function setDateNaiss(\DateTimeInterface $dateNaiss): self
    {
        $this->dateNaiss = $dateNaiss;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image)
    {
        $this->image = $image;

        return $this;
    }

    public function getProfil(): ?Profil
    {
        return $this->profil;
    }

    public function setProfil(?Profil $profil): self
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setUser($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getUser() === $this) {
                $photo->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setUser($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->contains($album)) {
            $this->albums->removeElement($album);
            // set the owning side to null (unless already changed)
            if ($album->getUser() === $this) {
                $album->setUser(null);
            }
        }

        return $this;
    }

    /****************************************************************/
    /******************Gestion d'ami et invitation*******************/

    /**
     * @return Collection|Amis[]
     */
    public function getAmis(): Collection
    {
        $ListAmis = new ArrayCollection();
        foreach($amis as $ami){
            if($ami->getAccepter() == 1)
                $ListAmis [] = $ami;
        }

        return $ListAmis;
    }

    /**
     * @return Collection|Amis[]
     */
    public function getInvitation(): Collection
    {
        $ListInvitation = new ArrayCollection();
        foreach($amis as $inviter){
            if($inviter->getAccepter() == 0)
                $ListInvitation [] = $inviter;
        }

        return $ListInvitation;
    }

    public function accepterAmi(Amis $ami): self
    {
        if(($this->amis->contains($ami)) && ($ami->getAccepter() == 0))
            $ami->setAccepter(1);
        return $this;
    }

    public function inviterAmi(Amis $ami): self
    {
        if (!$this->amis->contains($ami)) {
            $this->amis[] = $ami;
            $ami->setUserSource($this);
            $ami->setAccepter(0);
        }
        return $this;
    }

    public function removeAmiInvitation(Amis $ami): self
    {
        if ($this->amis->contains($ami)) {
            $this->amis->removeElement($ami);
            // set the owning side to null (unless already changed)
            if ($ami->getUserSource() === $this) {
                $ami->setUserSource(null);
            }
        }

        return $this;
    }
    /****************************************************************/

    /**
     * @return Collection|Amis[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(Amis $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->setUserTarget($this);
        }

        return $this;
    }

    public function removeUser(Amis $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getUserTarget() === $this) {
                $user->setUserTarget(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setUser($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            // set the owning side to null (unless already changed)
            if ($commentaire->getUser() === $this) {
                $commentaire->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Signaler[]
     */
    public function getSignalers(): Collection
    {
        return $this->signalers;
    }

    public function addSignaler(Signaler $signaler): self
    {
        if (!$this->signalers->contains($signaler)) {
            $this->signalers[] = $signaler;
            $signaler->setUser($this);
        }

        return $this;
    }

    public function removeSignaler(Signaler $signaler): self
    {
        if ($this->signalers->contains($signaler)) {
            $this->signalers->removeElement($signaler);
            // set the owning side to null (unless already changed)
            if ($signaler->getUser() === $this) {
                $signaler->setUser(null);
            }
        }

        return $this;
    }

    public function getCompte(): ?string
    {
        return $this->compte;
    }

    public function setCompte(?string $compte): self
    {
        $this->compte = $compte;

        return $this;
    }


}
