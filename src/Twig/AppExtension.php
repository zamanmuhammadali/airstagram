<?php

namespace App\Twig;


use App\Repository\AlbumRepository;
use App\Repository\AmisRepository;
use App\Repository\CommentaireRepository;
use App\Repository\PhotoRepository;
use App\Repository\SignalerRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $albumRep;
    private $photoRep;
    private $tagRep;
    private $userRep;
    private $amiRep;
    private $signalerRep;
    private $commentaireRep;


    public function __construct(SignalerRepository  $signalerRep ,CommentaireRepository  $commentaireRep,AmisRepository  $amiRep,UserRepository  $userRepo,TagRepository  $tagRep,PhotoRepository  $photoRep,AlbumRepository $albumRep)
    {
        $this->albumRep = $albumRep;
        $this->photoRep = $photoRep;
        $this->tagRep = $tagRep;
        $this->userRep = $userRepo;
        $this->amiRep = $amiRep;
        $this->signalerRep = $signalerRep;
        $this->commentaireRep = $commentaireRep;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('myFiltre', [$this, 'myFiltre']),
        ];
    }
    public function getFunctions()
    {
        return [
            new TwigFunction('getAlbum', [$this, 'getAlbum']),
            new TwigFunction('getPhoto', [$this, 'getPhoto']),
            new TwigFunction('photoDeleted', [$this, 'photoDeleted']),
            new TwigFunction('amiNombre', [$this, 'amiNombre']),
            new TwigFunction('invitationNombre', [$this, 'invitationNombre']),
            new TwigFunction('tag', [$this, 'tag']),
            new TwigFunction('getUserByPhoto', [$this, 'getUserByPhoto']),
            new TwigFunction('ifAmi', [$this, 'ifAmi']),
            new TwigFunction('ifInvite', [$this, 'ifInvite']),
            new TwigFunction('commentaireByTof', [$this, 'commentaireByTof']),
            new TwigFunction('tofSignaler', [$this, 'tofSignaler']),
        ];
    }
    public function getAlbum($user)
    {
        return $this->albumRep->findAlbumByUser($user);
    }
    public function getPhoto($user)
    {
        return $this->photoRep->findPhotoByUser($user,true);
    }
    public function photoDeleted($user)
    {
        return count($this->photoRep->findPhotoByUser($user,false));
    }
    /****/
    public function amiNombre($user)
    {
        return count($this->amiRep->findFriends($user));
    }
    public function invitationNombre($user)
    {
        return count($this->amiRep->findInvitation($user));
    }
    /***/
    public function tag()
    {
        return $this->tagRep->findAll();
    }
    public function getUserByPhoto($photoId)
    {
        return $this->userRep->getUserByPhoto($photoId)[0]['email'];
    }
    public function ifAmi($email, $idUserTarget)
    {
        $userSource = $this->userRep->findOneBy(['email'=>$email]);
        $data = $this->amiRep->findFriends($userSource);
        $userTarget = $this->userRep->findOneBy(['id'=>$idUserTarget]);

        if ($data) {
            foreach ($data as $value) {
                if ($userSource == $value->getUserTarget() &&  $userTarget == $value->getUserSource() && $value->getAccepter()==1){
                    return true;
                }  else if ($userSource == $value->getUserSource() && $userTarget == $value->getUserTarget() && $value->getAccepter()==1){
                    return true;
                }
            }
        }
        return false;
    }

    public function ifInvite($email, $idUserTarget)
    {
        $userSource = $this->userRep->findOneBy(['email'=>$email]);
        $data1 = $this->amiRep->findInvitation($userSource);
        $userTarget = $this->userRep->findOneBy(['id'=>$idUserTarget]);
        $data2 = $this->amiRep->findInvitation($userTarget);
        if ($data1) {
            foreach ($data1 as $value1) {
                if ($userSource == $value1->getUserTarget() &&  $userTarget == $value1->getUserSource() && $value1->getAccepter()==0){
                    return true;
                }  else if ($userSource == $value1->getUserSource() && $userTarget == $value1->getUserTarget() && $value1->getAccepter()==0){
                    return true;
                }
            }
        }
        if ($data2) {
            foreach ($data2 as $value2) {
                if ($userSource == $value2->getUserTarget() &&  $userTarget == $value2->getUserSource() && $value2->getAccepter()==0){
                    return true;
                }  else if ($userSource == $value2->getUserSource() && $userTarget == $value2->getUserTarget() && $value2->getAccepter()==0){
                    return true;
                }
            }
        }
        return false;
    }

    public function commentaireByTof($photoId)
    {
        $photo = $this->photoRep->findBy(['id'=> $photoId]);
        if ($photo) $photo= $photo[0];
        return $this->commentaireRep->findBy(['photo'=>$photo]);
    }

    public function tofSignaler()
    {
        $photo = count($this->signalerRep->findPhotoSignaler());
        return $photo;
    }
}
