<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findByEmailOrPseudo($email)    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT u
             FROM App\Entity\User u
             WHERE u.email =:email
             OR  u.pseudo=:pseudo '
             )->setParameter('email', $email)
              ->setParameter('pseudo', $email);
        return $query->getResult();
    }

    /**
     * @param $valeur
     * @return array|int|mixed|string
     */
    public function searchUser($valeur)
    {
        $qb = $this->createQueryBuilder('u')
            -> where( "u.id != -1");
        $users = $qb->getQuery()->execute();
        $userSearch = [];
        if($valeur==null){
            return $users;
        }else{
            foreach ($users as $user){
                $valeurUser = $user->getUsername() . $user->getNom() . $user->getPrenom() . $user->getPseudo();
                if(strpos($valeurUser,$valeur) !== false ){
                    $userSearch [] = $user;
                }
            }
            return $userSearch;
        }
    }

    public function getUserByPhoto($photoId)
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT u.email
        FROM App\Entity\User u
        JOIN u.photos tof
        WHERE tof.id =:id'
        )->setParameter('id', $photoId);
        return $query->getResult();
    }
}
