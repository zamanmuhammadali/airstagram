<?php

namespace App\Repository;

use App\Entity\Amis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Amis|null find($id, $lockMode = null, $lockVersion = null)
 * @method Amis|null findOneBy(array $criteria, array $orderBy = null)
 * @method Amis[]    findAll()
 * @method Amis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AmisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Amis::class);
    }

    public function findFriends($user)
    {
        $qb = $this->createQueryBuilder('a')
            -> where( "a.user_source =:user")
            -> orWhere ("a.user_target=:user" )
            -> andWhere( "a.accepter = 1")
            -> setParameter('user', $user);
        return $qb->getQuery()->execute();
    }


    public function searchFriends($user,$valeur)
    {
        $friends = $this->findFriends($user);
        foreach ($friends as $friend){
            $friendSearch = [];
            $valeurFriend = $friend->getUsername() . $friend->getNom() . $friend->getPrenom() . $friend->getPseudo();
            if(strpos($valeurFriend,$valeur) !== false){
                $friendSearch [] = $friend;
            }
        }
        return $friendSearch;
    }
    public function searchInvitations($user,$valeur)
    {
        $invitations = $this->findInvitation($user);
        foreach ($invitations as $invite){
            $inviteSearch = [];
            $valeurInvite = $invite->getUsername() . $invite->getNom() . $invite->getPrenom() . $invite->getPseudo();
            if(strpos($valeurInvite,$valeur) !== false){
                $inviteSearch [] = $invite;
            }
        }
        return $inviteSearch;
    }

    public function findInvitation($user)
    {
        $qb = $this->createQueryBuilder('a')
            -> where("a.user_target=:user")
            -> andWhere( "a.accepter = 0")
            -> setParameter('user', $user);
        return $qb->getQuery()->execute();
    }

}

