<?php

namespace App\Repository;

use App\Entity\Photo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Photo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Photo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Photo[]    findAll()
 * @method Photo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Photo::class);
    }

    public function findPhotoByUser($user,$etat)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT tof
        FROM App\Entity\Photo tof
        JOIN tof.user u
        WHERE u.email =:email
         AND tof.visible =:visible  
         AND tof.supprimer =:supprimer'
        )->setParameter('email', $user)
            ->setParameter('visible', $etat)
            ->setParameter('supprimer', false);

        return $query->getResult();
    }
    public function findPhotoByUserAndAlbum($user,$idAlbum)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT tof
        FROM App\Entity\Photo tof
        JOIN tof.user u
        JOIN tof.album al
        WHERE u.email =:email
        AND al.id =:idAlbum 
        AND tof.visible =:visible 
        AND tof.supprimer =:supprimer '
        )->setParameter('email', $user)
            ->setParameter('idAlbum', $idAlbum)
            ->setParameter('visible', 1)
            ->setParameter('supprimer', 0);
        return $query->getResult();
    }
    public function findPhotoByTag($user,$tagId)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT tof
        FROM App\Entity\Photo tof
        JOIN tof.user u
        JOIN tof.tag tag
        WHERE u.email =:email
        AND tag.id =:tagId 
        order by tof.createdAt DESC '
        )->setParameter('email', $user)
            ->setParameter('tagId', $tagId);
        return $query->getResult();
    }
    public function trieByDate($user)
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT tof
        FROM App\Entity\Photo tof
        JOIN tof.user u
        WHERE u.email =:email
        order by tof.createdAt ASC '
        )->setParameter('email', $user);
        return $query->getResult();
    }
    public function trieByNom($user)
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT tof
        FROM App\Entity\Photo tof
        JOIN tof.user u
        WHERE u.email =:email
        order by tof.image ASC '
        )->setParameter('email', $user);
        return $query->getResult();
    }


    // /**
    //  * @return Photo[] Returns an array of Photo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Photo
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
