<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\AmisRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use mysql_xdevapi\DatabaseObject;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/AIRstagram/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     * Ilyes
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * Ilyes
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     * Ilyes
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * Ilyes
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/search", name="user_search",methods={"GET","POST"})
     * @param Request $request
     * @param UserRepository $userRepository
     * @param SerializerInterface $serializer
     * @param PaginatorInterface $paginator
     * @return Response
     * Ilyes
     */
    public function search(Request $request, UserRepository $userRepository, SerializerInterface $serializer,PaginatorInterface  $paginator)
    {

        $value = $request->request->get('search');
        $datas = $userRepository->searchUser($value);
        $users = [];
        if ($datas) {
            foreach ($datas as $value) {
                if($value != $this->getUser() && $value->getpseudo()!= "admin"){
                    $data = $serializer->serialize($value, 'json', ['groups' => 'amis']);
                    $user = $serializer->deserialize($data, User::class, 'json');
                    $users [] = $user;
                }
            }
        }
        $pagination = $paginator->paginate(
            $users, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );

        $resultat =count($users);
        return $this->render('user/user_search.html.twig', [
            'users' => $pagination,
            'resultat' => $resultat,
        ]);
    }

}
