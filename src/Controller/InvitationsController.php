<?php

namespace App\Controller;

use App\Entity\Amis;
use App\Entity\User;
use App\Repository\AmisRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/AIRstagram/invitations")
 */
class InvitationsController extends AbstractController
{
    /**
     * @Route("/", name="invitations")
     * @param AmisRepository $amisRepository
     * @return Response
     * Ilyes
     */
    public function index(AmisRepository $amisRepository): Response
    {
        $user = $this->getUser();
        $data = $amisRepository->findInvitation($user);
        $invitation = [];
        if ($data) {
            foreach ($data as $value) {
                $invitation [] = $value->getUserSource();
            }
        }
        return $this->render('user/invitations/index.html.twig', [
            'invitation' => $invitation

        ]);
    }

    /**
     * @Route("/invite/{id}", name="invite_ami")
     * @param FlashyNotifier $notifier
     * @param User $user_target
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $manager
     * @param AmisRepository $amisRepository
     * @return RedirectResponse
     * Ilyes
     */
    public function inviteFriend(FlashyNotifier  $notifier,User $user_target ,UserRepository   $userRepository, EntityManagerInterface  $manager,AmisRepository  $amisRepository)
    {
        $username= $this->getUser()->getUsername();
        $user_source = $userRepository->findOneBy(['email'=>$username]);
        $invite = new Amis();
        $invite->setUserSource($user_source);
        $invite->setUserTarget($user_target);
        $invite->setAccepter(0);
        $manager->persist($invite);
        $manager->flush();
        $user = $this->getUser();
        $data = $amisRepository->findInvitation($user);
        $invites = [];
        if ($data) {
            foreach ($data as $value) {
                $invites [] = $value->getUserTarget();
            }
        }
        $notifier->success($user_target->getPseudo(). " a recu une invitation ! ");

        return $this->redirectToRoute('invitations');

    }

    /**
     * @Route("/accepter/{id}", name="accepter_invitation")
     * @param FlashyNotifier $notifier
     * @param User $user_source
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $manager
     * @param AmisRepository $amisRepository
     * @return RedirectResponse
     * Ilyes
     */
    public function accepteFriend(FlashyNotifier  $notifier,User $user_source ,UserRepository   $userRepository, EntityManagerInterface  $manager,AmisRepository  $amisRepository)
    {
        $user = $this->getUser();
        $invites = $amisRepository->findInvitation($user);
        foreach ($invites as $invite){
            if($invite->getUserSource() === $user_source){
                $invite->setAccepter(1);
            }
        }
        $manager->persist($invite);
        $manager->flush();

        $notifier->success("Vous avez accepter l'invitation de ".$user_source->getPseudo());

        return $this->redirectToRoute('invitations');
    }

    /**
     * @Route("/supprimer-invitation/{id}", name="supprimer_invitation")
     * @param FlashyNotifier $notifier
     * @param User $user_source
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $manager
     * @param AmisRepository $amisRepository
     * @return RedirectResponse
     * Ilyes
     */
    public function deleteInvitation(FlashyNotifier  $notifier,User $user_source ,UserRepository   $userRepository, EntityManagerInterface  $manager,AmisRepository  $amisRepository)
    {
        $username= $this->getUser()->getUsername();
        $user_target = $userRepository->findOneBy(['email'=>$username]);
        $invite = $amisRepository->findBy(['user_source'=>$user_source,'user_target'=>$user_target]);
        $manager->remove($invite[0]);
        $manager->flush();
        $notifier->success($user_source->getPseudo(). " a été supprimé de votre liste d'invites ");
        return $this->redirectToRoute('invitations');

    }

    /**
     * @Route("/retirer-invitation/{id}", name="retirer_invitation")
     * @param FlashyNotifier $notifier
     * @param User $user_target
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $manager
     * @param AmisRepository $amisRepository
     * @return RedirectResponse
     * Ilyes
     */
    public function retirerInvitation(FlashyNotifier  $notifier,User $user_target ,UserRepository   $userRepository, EntityManagerInterface  $manager,AmisRepository  $amisRepository)
    {
        $user_source = $this->getUser();
        $invite = $amisRepository->findBy(['user_source'=>$user_source,'user_target'=>$user_target]);
        $manager->remove($invite[0]);
        $manager->flush();
        $notifier->success( " l'invitation a été bien retirer ");
        return $this->redirectToRoute('profil');

    }
}
