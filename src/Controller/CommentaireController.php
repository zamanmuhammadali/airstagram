<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Repository\CommentaireRepository;
use App\Repository\PhotoRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CommentaireController extends AbstractController
{
    /**
     * @Route("/commentaire", name="commentaire")
     * Aminata
     */
    public function index(SerializerInterface  $serializer,Request  $request ,EntityManagerInterface  $manager,PhotoRepository   $photoRepository,UserRepository  $userRepository, CommentaireRepository  $commentaireRepository)
    {

        if ($request->isXmlHttpRequest()){
            $commentaire = new Commentaire();
            $username = $this->getUser()->getUsername();
            $user = $userRepository->findBy(['email'=>$username])[0];
            $photoId = $request->request->get('photoId');
            $photo = $photoRepository->findBy(['id'=>$photoId])[0];
            $message = $request->request->get('message');
            $commentaire->setUser($user);
            $commentaire->setPhoto($photo);
            $commentaire->setMessage($message);
            $commentaire->setDatePosted(new \DateTime());
            $manager->persist($commentaire);
            $manager->flush();
            $data = $commentaireRepository->findBy(['photo'=>$photo]);
            if ($data){
                $commentaires= $serializer->serialize($data, 'json',['groups' => 'commentaire']);
                return $this->json($commentaires,200);
            }
            $commentaires= $serializer->serialize($data, 'json',['groups' => 'commentaire']);
            return $this->json($commentaires,200);

        }

    }
}
