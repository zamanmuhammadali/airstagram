<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Repository\PhotoRepository;
use App\Repository\UserRepository;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/AIRstagram/corbeille")
 */
class CorbeilleController extends AbstractController
{
    /**
     * @Route("/", name="corbeille")
     * Aminata
     */
    public function index(PhotoRepository $photoRep, UserRepository $userRepository)
    {
        $username = $this->getUser()->getUsername();
        $photos = $photoRep->findPhotoByUser($username, false);

        if ($photos) {

            $date = new \DateTime();
            $date = $date->format('Y-m-d H:i:s');

            foreach ($photos as $value) {
                $dateSupprimer = $value->getDeletedAt();
                if ($dateSupprimer) {
                    $dateSupprimer = $dateSupprimer->format('Y-m-d H:i:s');
                    $dateLimite = date('Y-m-d', strtotime($dateSupprimer . ' + 30 days'));
                    if ($date >= $dateLimite) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $value->setSupprimer(true);
                        $entityManager->flush();
                        return $this->redirectToRoute('corbeille');
                    }
                }


            }
        }
        return $this->render('user/corbeille/index.html.twig', [
            'photos' => $photos,
        ]);
    }

    /**
     * @Route("/{id}", name="photo_delete",  methods={"DELETE"})
     * Aminata
     */
    public function delete(Request $request, Photo $photo, FlashyNotifier $notifier): Response
    {
        if ($this->isCsrfTokenValid('delete' . $photo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $photo->setSupprimer(true);
            $photo->setDeletedAt(new \DateTime());
            $entityManager->flush();
        }
        $notifier->success("l'image a été supprimé");
        return $this->redirectToRoute('corbeille');
    }

    /**
     * @Route("/restor/{id}", name="photo_restore",  methods={"get"})
     * Aminata
     */
    public function restore( Photo $photo, FlashyNotifier $notifier): Response
    {
        $photo->setVisible(true);
        $photo->setDeletedAt(null);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        $notifier->success("l'image a été restaurée");
        return $this->redirectToRoute('corbeille');
    }
}
