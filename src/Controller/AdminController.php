<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Entity\Signaler;
use App\Entity\User;
use App\Repository\AlbumRepository;
use App\Repository\PhotoRepository;
use App\Repository\SignalerRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/AIRstagram")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     * Aminata
     */
    public function index(SignalerRepository $signalerRepository, PhotoRepository $photoRep, UserRepository $userRep, AlbumRepository $albumRep)
    {
        $photos = count($photoRep->findAll());
        $users = $userRep->findAll();
        $albums = count($albumRep->findAll());
        $signaler = count($signalerRepository->findAll());
        return $this->render('admin/index.html.twig', [
            'photos' => $photos,
            'users' => $users,
            'albums' => $albums,
            'signaler' => $signaler
        ]);
    }

    /**
     * @Route("/album", name="admin_album")
     * Aminata
     */
    public function album(AlbumRepository $albumRep)
    {
        $albums = $albumRep->findAll();
        return $this->render('admin/album.html.twig', [

            'albums' => $albums
        ]);
    }

    /**
     * @Route("/signaler", name="admin_photo_signaler")
     * Aminata
     */
    public function photoSignaler(PaginatorInterface $paginator, Request $request, SignalerRepository $signalerRepository)
    {

        $photos = $paginator->paginate(
            $signalerRepository->findPhotoSignaler(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        return $this->render('admin/photo.signaler.html.twig', [

            'photos' => $photos
        ]);
    }

    /**
     * @Route("/photos", name="admin_photo")
     * Aminata
     */
    public function photo(Request $request, PaginatorInterface $paginator, PhotoRepository $photoRepository)
    {

        $photos = $paginator->paginate(
            $photoRepository->findBy(['supprimer' => 0]), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        return $this->render('admin/photo.html.twig', [
            'photos' => $photos
        ]);
    }

    /**
     * @Route("/photo/delete/{id}", name="admin_photo_delete",  methods={"DELETE"})
     * Aminata
     */
    public function deletePhoto(Request $request, Photo $photo, FlashyNotifier $notifier): Response
    {
        if ($this->isCsrfTokenValid('delete' . $photo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $photo->setSupprimer(true);
            $photo->setVisible(false);
            $photo->setDeletedAt(new \DateTime());
            $entityManager->flush();
        }
        $notifier->success("l'image a été supprimée");
        return $this->redirectToRoute('admin_photo');
    }

    /**
     * @Route("/photo/signaler/{id}", name="admin_photo_signaler_delete",  methods={"DELETE"})
     * Aminata
     */
    public function deletePhotoShignaler(int $id,SignalerRepository $signalerRepository, Request $request, Signaler $signaler, FlashyNotifier $notifier): Response
    {
        if ($this->isCsrfTokenValid('delete' . $signaler->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $signalers = $signalerRepository->findAll();

            if ($signalers) {
                foreach ($signalers as $photo) {
                  if ($photo->getId() == $id){
                      $photo->getPhoto()->setVisible(false);
                      $photo->getPhoto()->setSupprimer(true);
                      $photo->getPhoto()->setDeletedAt(new \DateTime());
                      $entityManager->remove($photo);
                      $entityManager->flush();
                  }
                }
            }

        }
        $notifier->success("l'image a été supprimée");
        return $this->redirectToRoute('admin_photo_signaler');
    }


    /**
     * @Route("/utilisateur", name="admin_user")
     * Aminata
     */
    public function utilisateur(UserRepository $userRepository, PaginatorInterface $paginator, Request $request)
    {
        $pagination = $paginator->paginate(
            $userRepository->findAll(),
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        return $this->render('admin/user.html.twig', [
            'users' => $pagination

        ]);

    }

}
