<?php

namespace App\Controller;

use App\Entity\Profil;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\ProfilRepository;
use App\Repository\UserRepository;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InscriptionController extends AbstractController
{
    private $encoder;
    /**
     * Aminata
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @Route("/inscription", name="inscription")
     * Aminata
     */
    public function new(Request $request, ProfilRepository $profilRepository, UserRepository $userRepository, FlashyNotifier $flashy): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $findEmail = $userRepository->findBy(['email' => $user->getEmail()]);
            $findPseudo = $userRepository->findBy(['pseudo' => $user->getPseudo()]);
            if ($findEmail) {
                $flashy->error('email: ' . $user->getEmail() . '  exite dèjà');
                return $this->render('user/new.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                ]);
            } else if ($findPseudo) {
                $flashy->error('pseudo: ' . $user->getPseudo() . '  exite dèjà');
                return $this->render('user/new.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                ]);

            }
            $profil = $profilRepository->findBy(['code' => 'US']);
            if ($profil) {
                $user->setProfil($profil[0]);
            }

            $password = $this->encoder->encodePassword($user, $user->getPassword());
            $confirmaton = $request->request->get('confirmation');
            if (strcmp($user->getPassword(), $confirmaton) != 0) {
                $flashy->error('les mots de passe ne sont pas identiques');
                return $this->render('user/new.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                ]);
            }
            $file = $form->getNormData()->getImageFile();

            if (!$file) {
               $user->setImage('user.png');
            }
            $user->setPassword($password);
            $user->setCompte('PUBLIC');
            $user->setUpdatedAt(new \DateTimeImmutable());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $flashy->success("l'inscription a réussi vous pouvez vous connecter avec votre email/pseudo et mot de passe");
            return $this->redirectToRoute('app_login');
        }
        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
