<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Entity\Tag;
use App\Repository\PhotoRepository;
use App\Repository\TagRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/AIRstagram")
 */
class TagController extends AbstractController
{
    /**
     * @Route("/tag/{id}", name="tag")
     * Barry
     */
    public function index(Request  $request,int $id, PhotoRepository $photoRepository ,PaginatorInterface  $paginator ,TagRepository  $tagRepository)
    {
        $tag =  $tagRepository->findOneBy(['id'=>$id])->getLibelle();
        $username = $this->getUser()->getUsername();
        $photos = $paginator->paginate(
            $photoRepository->findPhotoByTag($username, $id), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );
        return $this->render('user/tag/index.html.twig', [
            'photos' => $photos,
            'tag' => $tag
        ]);
    }
    /**
     * @Route("/trie", name="trie")
     * Barry
     */
    public function trie(SerializerInterface  $serializer,Request  $request, PhotoRepository $photoRepository ,PaginatorInterface  $paginator)
    {

            $trie = $request->request->get('select');
            $username = $this->getUser()->getUsername();
            $photos = [];
            if ($trie == "nom"){
               $datas = $photoRepository->trieByNom($username);
                if ($datas) {
                    foreach ($datas as $value) {
                        $data = $serializer->serialize($value, 'json', ['groups' => 'photos']);
                        $photo = $serializer->deserialize($data, Photo::class, 'json');
                        $photos [] = $photo;
                    }

                }
            }
            else if ($trie == "date"){
               $datas=  $photoRepository->trieByDate($username);
                if ($datas) {
                    foreach ($datas as $value) {
                        $data = $serializer->serialize($value, 'json', ['groups' => 'photos']);
                        $photo = $serializer->deserialize($data, Photo::class, 'json');
                        $photos [] = $photo;
                    }

                }

            }
        $pagination = $paginator->paginate(
           $photos, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );

        return $this->render('user/trie/index.html.twig', [
            'photos' => $pagination
        ]);

        }

}
