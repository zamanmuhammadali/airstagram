<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Photo;
use App\Entity\Tag;
use App\Form\AlbumType;
use App\Form\PhotoType;
use App\Repository\AlbumRepository;
use App\Repository\PhotoRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/AIRstagram/album")
 */
class AlbumController extends AbstractController
{
    /**
     * @Route("/", name="albums", methods={"GET"})
     * Ali
     */
    public function index(AlbumRepository $albumRepository, PaginatorInterface $paginator,Request $request): Response
    {
        $user = $this->getUser();

        $pagination = $paginator->paginate($albumRepository->findAlbumByUser($user->getUsername()), $request->query->getInt('page', 1), 9);
        return $this->render('user/album/index.html.twig', [
            'albums' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="album_new", methods={"GET","POST"})
     * Ali
     */
    public function new(Request $request, AlbumRepository $albumRep, UserRepository $userRepository, FlashyNotifier $flashyNotifier, EntityManagerInterface $manager): Response
    {
        $album = new Album();
        $userName = $this->getUser()->getUsername();
        if ($request->isXmlHttpRequest() && $request->request->get('nom')) {
            $nom = $request->request->get('nom');
            $inDatabase = $albumRep->findBy(['libelle' => $nom]);
            if ($inDatabase) {
                return $this->json(' existe');
            }
            $album->setVisible(true);
            $album->setCreatedAt(new \DateTime());
            $album->setLibelle($nom);
            $user = $userRepository->findBy(['email' => $userName]);
            if ($user) {
                $album->setUser($user[0]);
            }
            $manager->persist($album);
            $manager->flush();
            return $this->json('succees');
        }
    }


    /**
     * @Route("/photo/{id}/new", name="album_photo_new", methods={"GET","POST"})
     * Ali
     */
    public function newPhotoInAlbum(int $id, Request $request, AlbumRepository $albumRepo, UserRepository $userRepository, FlashyNotifier $flashy): Response
    {
        $photo = new Photo();
        $album = $albumRepo->findOneBy(['id' => $id]);
        if ($album) {
            $photo->setAlbum($album);
        }
        $username = $this->getUser()->getUsername();
        $user = $userRepository->findBy(['email' => $username]);
        $form = $this->createForm(PhotoType::class, $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            // verification de la format du fichier a uploader
            $mimeType = explode('/', $photo->getImageFile()->getMimeType());
            $types = ['jpeg', 'png', 'jpg'];
            if (!in_array($mimeType[1], $types)) {
                $flashy->error('type de fichier non prise en charge!');
                return $this->render('user/photo/new.html.twig', [
                    'photo' => $photo,
                    'form' => $form->createView(),
                ]);
            }
            if ($photo->getTag() == null) {
                if ($photo->getNewTag() == "") {
                    $flashy->error('le tag de l\'image ne doit pas etre vide ');
                    return $this->render('user/photo/new.html.twig', [
                        'photo' => $photo,
                        'form' => $form->createView(),
                    ]);
                } else {
                    $tag = new Tag();
                    $tag->setLibelle($photo->getNewTag());
                    $photo->setTag($tag);
                }
            }
            $photo->setUser($user[0]);
            $photo->setCreatedAt(new \DateTime());
            $photo->setVisible(true);
            $photo->setImage($photo->getImage());
            $photo->setSupprimer(false);
            $entityManager->persist($photo);
            $entityManager->flush();
            return $this->redirectToRoute('photos');
        }
        return $this->render('user/photo/new.html.twig', [
            'photo' => $photo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="album_show", methods={"GET"})
     * Ali
     */
    public function show(int $id,Request  $request,PaginatorInterface  $paginator,Album $album, PhotoRepository $photoRepository, UserRepository  $userRepository): Response
    {

        $username = $this->getUser()->getUsername();
        $pagination = $paginator->paginate(
            $photoRepository->findPhotoByUserAndAlbum($username, $id), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );

        return $this->render('user/album/show.html.twig', [
            'album' => $album,
            'photos' => $pagination
        ]);
    }

    /**
     * @Route("/{id}/edit", name="album_edit", methods={"GET","POST"})
     * Ali
     */
    public function edit(Request $request, Album $album): Response
    {
        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('album_index');
        }

        return $this->render('album/edit.html.twig', [
            'album' => $album,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="album_delete", methods={"DELETE"})
     * Ali
     */
    public function delete(Request $request, Album $album): Response
    {
        if ($this->isCsrfTokenValid('delete' . $album->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($album);
            $entityManager->flush();
        }

        return $this->redirectToRoute('album_index');
    }


}
