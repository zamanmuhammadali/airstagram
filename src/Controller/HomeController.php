<?php

namespace App\Controller;

use App\Repository\PhotoRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class HomeController extends AbstractController
{
    private $security;
    /**
     * Ali
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    /**
     * @Route("/AIRstagram/home", name="home")
     * Ali
     */
    public function index(PaginatorInterface  $paginator, PhotoRepository  $photoRepository,Request  $request)
    {
        $username = $this->getUser()->getUsername();
        $pagination = $paginator->paginate( $photoRepository->findPhotoByUser($username, true), $request->query->getInt('page', 1), 9);

        return $this->render('user/home/index.html.twig',[
            'photos'=>$pagination
        ]);
    }


}
