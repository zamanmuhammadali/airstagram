<?php
//A modifier

namespace App\Controller;

use App\Entity\Amis;

use App\Entity\User;
use App\Repository\AlbumRepository;
use App\Repository\AmisRepository;
use App\Repository\PhotoRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Knp\Component\Pager\PaginatorInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/AIRstagram/amis")
 */
class AmisController extends AbstractController
{
    /**
     * @Route("/", name="amis")
     * @param AmisRepository $amisRepository
     * @return Response
     * Ilyes
     */
    public function index(AmisRepository $amisRepository)
    {
        $user = $this->getUser();
        $data = $amisRepository->findFriends($user);
        $amis = [];
        if ($data) {
            foreach ($data as $value) {
                if ($user == $value->getUserTarget()){
                    $amis [] = $value->getUserSource();
                }  else{
                    $amis [] = $value->getUserTarget();

                }

            }
        }
        return $this->render('user/amis/index.html.twig', [
            'amis' => $amis

        ]);
    }

    /**
     * @Route("/search", name="amis_search",methods={"GET","POST"})
     * @param Request $request
     * @param AmisRepository $amisRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * Ilyes
     */
    public function search(Request $request, AmisRepository $amisRepository, SerializerInterface  $serializer)
    {

        if ($request->isXmlHttpRequest()) {
            $user = $this->getUser();
            $value = $request->get('search');
            $data = $amisRepository->searchFriends($user, $value);
        }
        if ($data){
            $amis= $serializer->serialize($data[0], 'json',['groups' => 'amis']);
            return $this->json($amis,200);
        }
        $amis= $serializer->serialize($data, 'json',['groups' => 'amis']);
        return $this->json($amis,200);
    }

    /**
     * @Route("/supprimer-ami/{id}", name="supprimer_ami")
     * @param FlashyNotifier $notifier
     * @param User $user_target
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $manager
     * @param AmisRepository $amisRepository
     * @return RedirectResponse
     * Ilyes
     */
    public function delete(FlashyNotifier  $notifier,User $user_target ,UserRepository   $userRepository, EntityManagerInterface  $manager,AmisRepository  $amisRepository)
    {
        $username= $this->getUser()->getUsername();
        $user_source = $userRepository->findOneBy(['email'=>$username]);
        $ami  = $amisRepository->findBy(['user_source'=>$user_source , 'user_target'=>$user_target]);
        if(count($ami)==0){
            $ami  = $amisRepository->findBy(['user_source'=>$user_target,'user_target'=>$user_source]);
        }
        //var_dump($ami);
        $manager->remove($ami[0]);
        $manager->flush();
        $notifier->success($user_target->getPseudo(). " a été supprimé de votre liste d'amis ");
        return $this->redirectToRoute('amis',[
        ]);

    }

    /**
     * @Route("/profil/{id}", name="profil_ami")
     * Ilyes
     */
    public function profil(AlbumRepository  $albumRepository,PaginatorInterface  $paginator ,Request  $request, User $user,PhotoRepository   $photoRep)
    {
        $albums = $albumRepository->findBy(['user'=>$user]);
        $pagination = $paginator->paginate(
            $photoRep->findPhotoByUser($user->getEmail(),true),
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );

        return $this->render('user/amis/profil.ami.html.twig', [
            'user'=>$user,
            'albums'=>$albums,
            'photos'=>$pagination

        ]);

    }

    /**
     * @Route("/profil/{idP}/album/{idAl}", name="profil_ami_album")
     * Ilyes
     */
    public function albumByprofil(AlbumRepository  $albumRepository,PaginatorInterface  $paginator ,Request  $request, PhotoRepository   $photoRep, UserRepository  $userRep)
    {
        $idUser  =$request->attributes->get('idP');
        $user= $userRep->findBy(['id'=>$idUser]);
        $albums = $albumRepository->findBy(['user'=>$user]);
        $idAlbum  =$request->attributes->get('idAl');
        $pagination = $paginator->paginate(
            $photoRep->findPhotoByUserAndAlbum($user[0]->getEmail(), $idAlbum), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );
        return $this->render('user/amis/profil.ami.html.twig', [
            'user'=>$user[0],
            'albums'=>$albums,
            'photos'=>$pagination

        ]);

    }


}
