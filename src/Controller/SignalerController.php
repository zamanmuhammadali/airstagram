<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SignalerController extends AbstractController
{
    /**
     * @Route("/signaler", name="signaler")
     * Aminata
     */
    public function index()
    {
        return $this->render('signaler/index.html.twig', [
            'controller_name' => 'SignalerController',
        ]);
    }
}
