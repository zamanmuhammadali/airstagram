<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Favorie;
use App\Repository\AlbumRepository;
use App\Repository\FavorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/AIRstagram/favorie")
 */
class FavorieController extends AbstractController
{


    /**
     * @Route("/", name="album_favorie", methods={"GET","POST"})
     * Barry
     */
    public function favorie(AlbumRepository $albumRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $user = $this->getUser();
        $pagination = $paginator->paginate(
            $albumRepository->findAlbumFavrieByUser($user->getUsername()), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );
        return $this->render('user/album/index.html.twig', [
            'albums' => $pagination,
        ]);
    }

    /**
     * @Route("/add/{id}", name="album_favorie_add", methods={"GET","POST"})
     * Barry
     */
    public function addFavorie(FlashyNotifier $notifier, Album $album, FavorieRepository $favorieRepository, AlbumRepository $albumRepository, PaginatorInterface $paginator, Request $request, EntityManagerInterface $manager): Response
    {
        $favories = $favorieRepository->findAll();
        if ($favories) {
            $album->setFavorie($favories[0]);
            $manager->persist($album);
            $manager->flush();
        } else {
            $favorie = new Favorie();
            $favorie->setCreatedAt(new \DateTime());
            $album->setFavorie($favorie);
            $manager->persist($album);
            $manager->flush();
        }
        $user = $this->getUser();
        $pagination = $paginator->paginate(
            $albumRepository->findAlbumFavrieByUser($user->getUsername()), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );
        $notifier->success('l\'album a été ajouté dans les favories');
        return $this->render('user/album/index.html.twig', [
            'albums' => $pagination,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="album_favorie_delete", methods={"GET","POST"})
     * Barry
     */
    public function deleteInFavorie(FlashyNotifier $notifier, Album $album, FavorieRepository $favorieRepository, AlbumRepository $albumRepository, PaginatorInterface $paginator, Request $request, EntityManagerInterface $manager): Response
    {

        $album->setFavorie(null);
        $manager->flush();
        $user = $this->getUser();
        $pagination = $paginator->paginate(
            $albumRepository->findAlbumFavrieByUser($user->getUsername()), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );
        $notifier->success('l\'album a été supprimé  des favories');
        return $this->render('user/album/index.html.twig', [
            'albums' => $pagination,
        ]);
    }

}
