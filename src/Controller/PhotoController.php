<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Entity\Signaler;
use App\Entity\Tag;
use App\Form\PhotoType;
use App\Repository\PhotoRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/AIRstagram/photos")
 */
class PhotoController extends AbstractController
{
    /**
     * @Route("/", name="photos", methods={"GET","POST"})
     * Barry
     */
    public function index(Request $request, PhotoRepository $photoRepository, PaginatorInterface $paginator)
    {
        $username = $this->getUser()->getUsername();
        $pagination = $paginator->paginate(
            $photoRepository->findPhotoByUser($username, true), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );

        return $this->render('user/photo/index.html.twig', [
            'photos' => $pagination
        ]);
    }


    /**
     * @Route("/new", name="photo_new", methods={"GET","POST"})
     * Barry
     */
    public function new(Request $request, UserRepository $userRepository, FlashyNotifier $flashy): Response
    {

        $photo = new Photo();
        $username = $this->getUser()->getUsername();
        $user = $userRepository->findBy(['email' => $username]);
        $form = $this->createForm(PhotoType::class, $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            // verification de la format du fichier a uploader
            $mimeType = explode('/', $photo->getImageFile()->getMimeType());
            $types = ['jpeg', 'png', 'jpg'];
            if (!in_array($mimeType[1], $types)) {
                $flashy->error('type de fichier non prise en charge!');
                return $this->render('user/photo/new.html.twig', [
                    'photo' => $photo,
                    'form' => $form->createView(),
                ]);
            }
            if ($photo->getTag() == null) {
                if ($photo->getNewTag() == "") {
                    $flashy->error('le tag de l\'image ne doit pas etre vide ');
                    return $this->render('user/photo/new.html.twig', [
                        'photo' => $photo,
                        'form' => $form->createView(),
                    ]);
                } else {
                    $tag = new Tag();
                    $tag->setLibelle($photo->getNewTag());
                    $photo->setTag($tag);
                }
            }
            $photo->setUser($user[0]);
            $photo->setCreatedAt(new \DateTime());
            $photo->setVisible(true);
            $photo->setSupprimer(false);
            $photo->setImage($photo->getImage());
            $entityManager->persist($photo);
            $entityManager->flush();
            return $this->redirectToRoute('photos');
        }

        return $this->render('user/photo/new.html.twig', [
            'photo' => $photo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="photo_archiver", methods={"PUT"})
     * Barry
     */
    public function archiver(Request $request, Photo $photo, FlashyNotifier $notifier): Response
    {
        if ($this->isCsrfTokenValid('delete' . $photo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $photo->setVisible(false);
            $entityManager->flush();
            $notifier->success("l'image a été déplacée dans la corbeille ");
            return $this->redirectToRoute('photos');
        }
    }

    /**
     * @Route("/signaler", name="signaler_photo", methods={"POST","GET"})
     * Barry
     */
    public function signaler(Request  $request,FlashyNotifier  $notifier, EntityManagerInterface  $manager,PhotoRepository $photoRepository , UserRepository  $userRepository)
    {
        if ($request->isXmlHttpRequest()){
            $username = $this->getUser()->getUsername();
            $idPhoto = $request->request->get('idPhoto');
            $photo = $photoRepository->findBy(['id'=>$idPhoto]);
            $user = $userRepository->findBy(['email'=>$username]);
            $signal = new Signaler();
            $signal->setUser($user[0]);
            $signal->setPhoto($photo[0]);
            $signal->setDateSignal(new  \DateTime());
            $manager->persist($signal);
            $manager->flush();
            $notifier->message('le photo a été signaler, l\'administrateur va le vérifier');
            return  $this->json('success',200);
        }


    }

}
