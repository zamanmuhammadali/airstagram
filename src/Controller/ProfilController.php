<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Vich\UploaderBundle\Entity\File;
use function Composer\Autoload\includeFile;

/**
 * @Route("/AIRstagram")
 */
class ProfilController extends AbstractController
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    /**
     * @Route("/profil", name="profil")
     * Ali
     */
    public function index()
    {
        return $this->render('user/profil/index.html.twig');
    }

    /**
     * @Route("/profil/edit", name="profil_edit", methods={"post","get"})
     * Ali
     */
    public function edit(Request $request, FlashyNotifier $notifier, UserRepository $userRepository): Response
    {
        $username = $this->getUser()->getUsername();
        $user = $userRepository->findOneBy(['email' => $username]);
        $passworduser  = $user->getPassword();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $request->request->get('user');
            if ($data['prenom']) {
                $user->setPrenom($data['prenom']);
            }
            if ($data['nom']) {
                $user->setNom($data['nom']);
            }
            if ($data['email']) {
                $user->setEmail($data['email']);
            }
            if ($data['pseudo']) {
                $user->setPseudo($data['pseudo']);
            } if ($data['adresse']) {
                $user->setAdresse($data['adresse']);
            }
            $user->setPassword($passworduser);
            if (!empty($data['password'])){
                $password = $data['password'];
                $confirmation = $request->request->get('confirmation');
                //verification si le mot de passe actuel correspond au mot de passe saisie
                    if ($password != $confirmation) {
                        $notifier->error('les mots de passe que vous avez saisie ne correspondent pas ');
                        return $this->render('user/profil/edit.html.twig', [
                            'user' => $user,
                            'form' => $form->createView(),
                        ]);
                    }
                    $password = $this->encoder->encodePassword($user,$password);
                    $user->setPassword($password);


            }

            $this->getDoctrine()->getManager()->flush();
            $notifier->warning('les information de votre profile ont été modifié');
            return $this->redirectToRoute('profil');
        }

        return $this->render('user/profil/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/etat_compte", name="etat_compte")
     * Ali
     */
    public function etaCompte(Request $request, UserRepository $userRepository, EntityManagerInterface $manager)
    {

        if ($request->isXmlHttpRequest()) {
            $etat = $request->get('etat');
            $username = $this->getUser()->getUsername();
            $user = $userRepository->findBy(['email' => $username])[0];
            if ($etat == 'on')
                $user->setCompte('PRIVATE');

            else   $user->setCompte('PUBLIC');

            $manager->flush();

            return $this->json($etat, 200);
        }


    }

}
