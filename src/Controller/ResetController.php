<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetController extends AbstractController
{

    /**
     * @Route("/reset", name="reset_password",methods={"GET","POST"})
     * Aminata
     */
    public function reset()
    {
        return $this->render('user/reset_password/reset.password.html.twig');
    }

    /**
     * @Route("/new/password", name="new_password",methods={"GET","POST"})
     * Aminata
     */
    public function newPassword(EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder, Request $request, FlashyNotifier $notifier, UserRepository $userRepository, \Swift_Mailer $mailer)
    {
        $email = $request->get('email');

        $user = $userRepository->findByEmailOrPseudo($email);
        if ($user) {
            //generer un   nouveau mot de passe
            $cars = "azertyiopqsdfghjklmwxcvbn0123456789";
            $password = '';
            $taille = strlen($cars);
            srand((double)microtime() * 1000000);
            //Initialise le générateur de mot de passe aléatoires
            for ($i = 0; $i < $taille; $i++) {
                $password = $password . substr($cars, rand(0, $taille - 1), 1);
            }
            $passwordEncoder = $encoder->encodePassword($user[0], $password);
            $user[0]->setPassword($passwordEncoder);
            $msg = (new \Swift_Message('réinitialisation complete AIRstagram  '))
                ->setFrom('AIRstagram@gmail.com')
                ->setTo($user[0]->getEmail())
                ->setBody(
                    $password . " est votre nouveau mot de passe. Vous pouvez le modifier une fois connecté :
                          Merci de votre fidélité AIRstagram");
            $mailer->send($msg);
            $manager->flush();
            $notifier->message('consultez votre courier mail pour voir le nouneau mot de passe');
            return $this->redirectToRoute('app_login', [
                'error' => null,
                "last_username" => $user[0]->getEmail()
            ]);

        }

        $notifier->message('email ou le pseudo innconnu');
        return $this->render('user/reset_password/reset.password.html.twig');

    }

}
