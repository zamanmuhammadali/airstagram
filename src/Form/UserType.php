<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichFileType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',TextType::class,[
                'required' => true,
                'invalid_message' => 'Veillez entrez un email valide',
            ])
            ->add('password',PasswordType::class,[
                'required'=> false,

            ])

            ->add('nom',TextType::class,[
                "required"=>true
            ])
            ->add('prenom',TextType::class,[
                "required"=>true
            ])
            ->add('pseudo',TextType::class,[
                "required"=>true
            ])
            ->add('adresse',TextType::class,[
                "required"=>true
            ])
            ->add('dateNaiss',BirthdayType::class,[
                "label"=>"date de naissance",
            ])
            ->add('imageFile',VichFileType::class,[
                'required'=>false,
                'attr' => [
                    'placeholder'=>'image de profile',
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '4024k',
                        /*   'mimeTypes' => [
                               'audio/mp3',
                               'audio/mpeg',
                           ],*/
                        // 'mimeTypesMessage' => 'dugallal ndeegate gu baax ',
                        'maxSizeMessage' => 'tail de l\'image non autorisé ',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
