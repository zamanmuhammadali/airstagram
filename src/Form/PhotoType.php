<?php

namespace App\Form;

use App\Entity\Photo;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichFileType;

class PhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description',TextType::class,[
                'required' => true,
                'attr' => [
                    'placeholder'=>'description de votre image',
                ],
            ])
            ->add('image',TextType::class,[
                'required' => true,
                'attr' => [
                    'placeholder'=>'Nom de votre image',
                ],
            ])
            ->add('tag', EntityType::class, [
                'class' => Tag::class,
                'choice_label' => 'libelle',
                'placeholder' => '',
                'required'=>false

            ])
            ->add('newTag', textType::class, [
                'required' => false,
                'attr' => [
                    'placeholder'=>'créer votre tag',
                ],

            ])
            ->add('imageFile',VichFileType::class,[
                'required'=>true,
                'attr' => [
                    'placeholder'=>'ajouter un image',
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '4024k',
                        'maxSizeMessage' => 'taille de l\'image non autorisé ',
                    ])
                ],
            ])
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Photo::class,
        ]);
    }
}
